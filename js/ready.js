var attributes;

$(document).ready(function(){
    $('#customerSelect').multiselect();


    // test data
    $('#boxModal').modal('show');
    attributes = {
        code : 'test code',
        referenceName : 'Rail Stack 1',
        description : 'description',
        rowNumbers : 23,
        colNumbers : 6,
        tierNumbers : 4,
        hasContainer : true
    }
    setupModal(attributes);
});



function getRandomColor() {
  
  var r = Math.floor((Math.random() * 255) + 1);
  var g = Math.floor((Math.random() * 255) + 1);
  var b = Math.floor((Math.random() * 255) + 1);
  
  var color = r+","+g+","+b;
  console.log(color);
  return color;
}

$(document).off('mouseenter','.grid-container').on('mouseenter','.grid-container',function(){
    $('.grid-container[count="'+$(this).attr('count')+'"]').addClass('select-col');
}).on('mouseleave','.grid-container',function(){
	$('.grid-container').removeClass('select-col');
})



$(document).on('click','.container-numbers-search li',function(){

    $('.containersearch').val($(this).attr('data-number'));
    $('.container-numbers-search').hide();
    
    var boxCount  = $('.box').length;
    var random = Math.floor((Math.random() * boxCount) - 1);
    
    $('.box').eq(random).addClass('contains-container');
    $('.box').not('.contains-container').addClass('fade-out-stack');

});

$(document).on('click','.select-col',function(){

    var containerList = [];

    $('.grid-container').removeClass('selected-col');
    $('.select-col').addClass('selected-col');
    $('.selected-col').each(function(){
        containerList.push({
            'fillCount' : $(this).attr('fill-count'),
            'containsContainer' : $(this).hasClass('grid-contains-container')
        })
    });

    console.log(containerList)

    viewContainers(containerList);

});

function viewContainers(containerList) {
    
    $('.view-container').html('').removeClass('d-none');
    
    var htmlString = ""
    $.each(containerList,function(i,v){

        var randomContains;
        
        if (v.containsContainer) {
            randomContains = Math.floor((Math.random() * v.fillCount));
        }

        //tier numbers
        if (i === 0){
            htmlString = htmlString + '<div class="tier-col">'
            var decline = (attributes.tierNumbers - 1);
            for(var xx = 0; xx < attributes.tierNumbers; xx++){
                htmlString = htmlString + '<div class="tier-num">' + decline + '</div>';
                decline = decline - 1;
            };

            htmlString = htmlString + '</div>'
            
        }

        htmlString = htmlString + '<div class="con-col">'
        var fullcount = 0;
        for(var x = 0; x < attributes.tierNumbers; x++){

            if (( attributes.tierNumbers - v.fillCount) <= x) {
                var random =  Math.floor((Math.random() * 4) + 1);
                
                if (fullcount == randomContains) {
                    htmlString = htmlString + '<div class="full-container contains-container-view con'+random+'">'
                    fullcount++
                } else {
                    htmlString = htmlString + '<div class="full-container con'+random+'">'
                    fullcount++
                }
                htmlString = htmlString + '</div>'
            } else {
                htmlString = htmlString + '<div class="empty-container"></div>'
            }
        }

        htmlString = htmlString + '<div class="col-num">'+i+'</div>'        

        htmlString = htmlString + '</div>'
        $('.view-container').html(htmlString);
    });
    assignContainerNumbers();
}

function assignContainerNumbers(){

    $.getJSON('/stub/containers.json', function(data) {
        var dataset = data.containers;
        var x = 0;

        $('.full-container').each(function(i,v){
            // var randomise = Math.floor((Math.random() * dataset.length));
            var randomise = x;
            var containerNumber = dataset[randomise].containerNumber;
            var ISOCode = dataset[randomise].ISOCode;
            var weight = dataset[randomise].GrossWeight;
            var cargoType = dataset[randomise].CargoType;
            var customerName = dataset[randomise].customerName;

            var htmlString = '<div class="quick-info-container">'+containerNumber+'<br>'+ISOCode+' &nbsp; &nbsp; '+weight+' <br> <span class="customer-name-short">' + customerName + '</span></div>';
            $(this).html(htmlString);
            $(this).attr('data-index',randomise);            
            x++
        });
        
    });

}


$(document).on('click','.full-container',function(){

    $('.selected-col-tier').removeClass('.selected-col-tier');
    $(this).parent().addClass('selected-col-tier');

    var row = parseInt($('.selected-col').first().attr('count')) - 1;
    var col = $(this).parent().find('.col-num').text();
    var tierNumber = $('.selected-col-tier > div').index(this);
    var tier = $('.tier-col .tier-num').eq(tierNumber).text();
    
    var htmlString = 'Row : ' + row + ' / Col : ' + col + ' / Tier : ' + tier;

    $('.container-locations').html(htmlString);

    $('.container-details').removeClass('d-none');
    $('.full-container').removeClass('container-selected');
    $(this).addClass('container-selected');

    loadContainerDetails($(this).attr('data-index'));

});

function loadContainerDetails(dataIndex){

    $.getJSON('/stub/containers.json', function(data) {
        var dataSet = data.containers[dataIndex];

        $.each(dataSet,function(i,v){
            if (v === "") {
                $(".bind-data[data-bind='"+i+"']").parent().hide();
            } else {
                $(".bind-data[data-bind='"+i+"']").html(v);
                $(".bind-data[data-bind='"+i+"']").parent().show();
            }
                
        });
    })

};


$(document).on('click','.save-modal-stack',function(){

    $('.current-stack.box-wrapper').draggable("destroy")
    $('.current-stack .box').rotatable('destroy');
    $('.current-stack .box').resizable( "destroy" );
    
    var code = $('#code').val();
    var referenceName = $('#referenceName').val();
    var description = $('#description').val();
    var rowNumbers = $('#rowNumbers').val();
    var colNumbers = $('#colNumbers').val();
    var tierNumbers = $('#tierNumbers').val();

    $('.current-stack .box').attr({
        'title' : referenceName,
        'code' : code,
        'referenceName' : referenceName,
        'description' : description,
        'rowNumbers' :rowNumbers,
        'colNumbers' : colNumbers,
        'tierNumbers' : tierNumbers
    });
    $('.current-stack .box').tooltip();

    $('.box-wrapper').removeClass('current-stack');
    $('.save-stack, .color-select').addClass('d-none');    
    $('.complete-layout').removeClass('d-none');
    

});

$(document).on('click','.save-stack',function(){
    $('#stack-settings').modal('show');
});

function update(picker) {
    var randomColor = Math.round(picker.rgb[0]) + ', ' + Math.round(picker.rgb[1]) + ', ' + Math.round(picker.rgb[2]);
    $('.current-stack .box').css({
        'background-color' : 'rgba('+randomColor+',0.7)',
        'border' : '3px solid rgba('+randomColor+',1)'
    })

}

$(document).on('click','.complete-layout:not(".complete")',function(){
    $('.create-stack, .complete-layout').addClass('complete');
    $('.right-nav ol, h1').slideUp();
    $('.container-search').removeClass('d-none').slideUp(0).slideDown();

});


$(document).on('keyup','.containersearch',function(){

    $('.box').removeClass('fade-out-stack').removeClass('contains-container');
    $('.grid-contains-container').removeClass('grid-contains-container');
    $('.contains-container-view').removeClass('contains-container-view');
    

    $.getJSON('/stub/containers.json', function(data) {
        $('.results').html('<ul class="container-numbers-search"></ul>');

        var count = 0;

        if ($('.containersearch').val() !== "") {
            $.each(data.containers,function(i,v){
                if (count < 5 ) {
                    if (v.containerNumber.indexOf($('.containersearch').val().toUpperCase()) >= 0) {
                        $('.results ul').append('<li data-number="'+v.containerNumber+'">'+v.containerNumber+' <i class="fa fa-search"></i></li>');
                        count++;
                    }
                }
            });
        }

    })
});

$(document).on('click','.create-stack:not(".complete")',function(){
    

    if ($('.current-stack').length > 0) {
        $('.save-stack').addClass('btn-error');
        setTimeout(function(){
            $('.save-stack').removeClass('btn-error')
        },600)
        return false;
    }

    $('.complete-layout').addClass('d-none');
    
    $('.save-stack, .color-select').removeClass('d-none');    

    var randomNumber = Math.floor((Math.random() * 999) + 999);
    var randomColor = getRandomColor();

    $('.jscolor').val(randomColor);

    // Prepare extra handles
    var ne = $("<div>", {class: "ui-rotatable-handle"});

    $('<div class="box-wrapper current-stack" id="wrap-'+randomNumber+'"><div data-toggle="tooltip" class="box" id="box-'+randomNumber+'"></div></div>').appendTo('body');

    // Assign Draggable
    $('#wrap-'+randomNumber).draggable({cancel: ".ui-rotatable-handle"});

    // Assign Rotatable
    $('#box-'+randomNumber).css({
        'background-color' : 'rgba('+randomColor+',0.7)',
        'border' : '3px solid rgba('+randomColor+',1)'
    }).resizable({
        handles: "n, e, s, w"
    }).rotatable();

    // Assign coordinate classes to handles
    $('#box-'+randomNumber+' div.ui-rotatable-handle').addClass("ui-rotatable-handle-sw");
    ne.addClass("ui-rotatable-handle-ne");

    // Assigning bindings for rotation event
    $("#box-"+randomNumber+" div[class*='ui-rotatable-handle-']").bind("mousedown", function(e) {$('#box-'+randomNumber).rotatable("instance").startRotate(e);});

});

$(document).on('click','.create-map:not(.complete)',function(){

    $('.right-nav').hide();
    $('.gmnoprint').hide();
    $('[title="Toggle fullscreen view"]').hide();

    if(window.chrome || navigator.userAgent.match('CriOS')) {// Fix for Chrome
        var transform=$(".gm-style>div:first>div").css("transform");
        var comp=transform.split(","); //split up the transform matrix
        var mapleft=parseFloat(comp[4]); //get left value
        var maptop=parseFloat(comp[5]);  //get top value
        $(".gm-style>div:first>div").css({ //get the map container. not sure if stable
        "transform":"none",
        "left":mapleft,
        "top":maptop,
        });
    }

    html2canvas(document.body, {
        useCORS: true,
        onrendered: function(canvas) {
            var mapWrapper = document.createElement("div");
            mapWrapper.setAttribute('id', 'map-wrapper');
            document.body.appendChild(mapWrapper);

            var boundaryWrapper = document.createElement("div");
            boundaryWrapper.setAttribute('id', 'boundary-wrapper');

            mapWrapper.appendChild(boundaryWrapper);
            mapWrapper.appendChild(canvas);
            if(window.chrome || navigator.userAgent.match('CriOS')) {// Fix for Chrome
                $(".gm-style>div:first>div").css({
                left:0,
                top:0,
                "transform":transform
                });
            }
        }
    });

    console.log($('#map'), map.getBounds().getNorthEast().toString(), map.getBounds().getSouthWest().toString());

    $('#map').remove();
    $('.create-boundary').removeClass('d-none');
    $('.right-nav').show();
    $(this).addClass('complete');

});

$(document).on('click','.create-boundary:not(.complete)',function(){
    $document = $(document);
    var $self = $(this);
    var boundaryWrapper = document.getElementById("boundary-wrapper");
    var boundary = newBoundary();

    $('.boundary-btn').removeClass('d-none'); 

    $document.on('click', '.reset-boundary', function(){
        boundary.draw('cancel');
        delete boundary;
        boundaryWrapper.innerHTML = "";
        boundary = newBoundary();
        console.log(boundary);
    });

    $document.on('click', '.save-boundary', function(){
        boundary.draw('done');
        boundary.off('drawstart');
        $self.addClass('complete');
        $('.create-stack').removeClass('d-none');
        $('.boundary-btn').addClass('d-none');
    });
    
    boundary.on('drawstop', function(){
        // remove listener
    });
});

var newBoundary = function(){
    return new SVG('boundary-wrapper').size('100%', '100%')
    .polygon()
    .draw()
    .attr('stroke-width', 2)
    .attr('stroke', '#333')
    .attr('fill', 'rgba(200, 200, 200, .3)');
}

$(document).on('click','.box:not(.ui-resizable)',function(){
    
    var _this = $(this);
    var containerSearch = false;
    if ($(this).hasClass('contains-container')) {
        containerSearch = true;
    }
    
    $('#boxModal').modal('show');

    attributes = {
        code : _this.attr('code'),
        referenceName : _this.attr('referenceName'),
        description : _this.attr('description'),
        rowNumbers : _this.attr('rowNumbers'),
        colNumbers : _this.attr('colNumbers'),
        tierNumbers : _this.attr('tierNumbers'),
        hasContainer : containerSearch
    }

    setupModal(attributes);

})



function setupModal(attributes) {

    var content = $('#boxModal .modal-body .container-grid');
        
        $('.stack-reference span').html(attributes.referenceName);
        $('.rows-counter span').html(attributes.rowNumbers);
        $('.columns-counter span').html(attributes.colNumbers);
        $('.tiers-counter span').html(attributes.tierNumbers);

        content.html('');
        var widths = 95 / attributes.rowNumbers + '%';

        var colcount = 0;

        console.log(attributes)

        //content
        for(var i = 0; i < (parseInt(attributes.colNumbers) + 1); i++){ 
            var row = document.createElement("div"); 
            if (i === 0) {
                row.className = "grid-row-header"; 
            } else {
                row.className = "grid-row"; 
            }
                

            for(var x = 1; x <= attributes.rowNumbers; x++){ 
                var cell = document.createElement("div"); 

                if (i === 0) {
                    cell.className = "grid-header"; 
                    cell.innerText = (i * attributes.rowNumbers) + x - 1;
                    cell.style.width = widths;

                } else if (i !== -1) {
                    cell.className = "grid-container"; 
                    cell.style.width = widths;
                    var randomFull = Math.floor(Math.random() * (parseInt(attributes.tierNumbers) + 1));
                    cell.innerText =  randomFull + "/" + attributes.tierNumbers;
                    var opac = 1 - (attributes.tierNumbers / randomFull) / 5;
                    cell.style.backgroundColor = "rgba(42,106,0,"+opac+")";
                    cell.setAttribute('count',x);
                    cell.setAttribute('fill-count',randomFull);
                }

                if (x === 1 && i !== -1 && i !== 0) {
                    var cell2 = document.createElement("div"); 
                    cell2.innerText = colcount;
                    cell2.className = "grid-col";
                    row.appendChild(cell2); 
                    colcount ++;
                }
                
                row.appendChild(cell); 
            } 
            content.append(row); 
        } 

        if (attributes.hasContainer === true) {
            doGridHighlight();
        }

}

function doGridHighlight() {
    var gridCount  = $('.grid-container').length;
    var random = Math.floor((Math.random() * gridCount) - 1);
    
    if ( parseInt($('.grid-container').eq(random).attr('fill-count')) === 0) {
        doGridHighlight();
        return false;
    }

    $('.grid-container').eq(random).addClass('grid-contains-container');
}

